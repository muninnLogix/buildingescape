// Copyright Blasphemer Brainchild Studios, 2022


#include "Grabber.h"
#include "DrawDebugHelpers.h"
#include "Engine/World.h"
#include "GameFramework/PlayerController.h"


#define OUT

// Sets default values for this component's properties
UGrabber::UGrabber()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
}


// Called when the game starts
void UGrabber::BeginPlay()
{
	Super::BeginPlay();

	const FString CallingActorName = GetOwner()->GetName();

	// Check for physics handle component
	PhysicsHandle = GetOwner()->FindComponentByClass<UPhysicsHandleComponent>();
	if ( IsNullComponent(PhysicsHandle, "PhysicsHandle", CallingActorName) )
	{
		return;
	}

	// Setup Player input component
	PlayerInput = GetOwner()->FindComponentByClass<UInputComponent>();
	if ( IsNullComponent(PlayerInput, "PlayerInput", CallingActorName) )
	{
		return;
	}

	// Can Get away with this because PlayerInput is included in AActor class now, so should never be null
	ConfigurePlayerInputBindings(PlayerInput);

}


// Check if pointer is null, UE_LOG with component name and component Owner if null
bool UGrabber::IsNullComponent(const void* component, const FString& testingComponent, const FString& ownerName) const
{
	if ( !component )
	{
		UE_LOG(LogTemp, Error, TEXT("Component '%s' on Actor '%s' failed to Initialize."), *testingComponent, *ownerName);
		return true;
	}

	return false;
}


void UGrabber::ConfigurePlayerInputBindings(UInputComponent* PlayerInputController)
{
	PlayerInputController->BindAction("Grab", IE_Pressed, this, &UGrabber::GrabItem);
	PlayerInputController->BindAction("Grab", IE_Released, this, &UGrabber::ReleaseItem);
}


void UGrabber::SetGrabberLineTraceEnd()
{
	/* If we can find First player controller
	 * Then set the component vars for player view point
	 * and set our line trace end
	 */
	APlayerController* FirstPlayerController = GetWorld()->GetFirstPlayerController();
	if ( FirstPlayerController )
	{
		FirstPlayerController->GetPlayerViewPoint(
			OUT PlayerViewpointLoc,
			OUT PlayerViewpointRot
		);

		GrabberLineTraceEnd = PlayerViewpointLoc + PlayerViewpointRot.Vector() * PlayerReach;
	}
}


void UGrabber::GetFirstPhysicsBodyInReach(FHitResult& playerRayTraceHit) const
{

	/* Casts a Ray out to GrabberLineTraceEnd
	 * If something is in between our viewpoint and the endpoint
	 * Then we check if it's a ECC_PhysicsBody and return it.
	 */

	const FCollisionQueryParams TraceParams = {
		FName(TEXT("")),
		false,
		GetOwner() // If we don't ignore the host component, will get ourselves each time
	};
	
	GetWorld()->LineTraceSingleByObjectType(
		OUT playerRayTraceHit,
		PlayerViewpointLoc,
		GrabberLineTraceEnd,
		FCollisionObjectQueryParams(ECollisionChannel::ECC_PhysicsBody),
		TraceParams	
	);
	
	/* If we have an actor, then we return that actor at the end
	 * but if not, we make sure to Set playerRayTraceHit to an empty FHit.
	 */
	if ( !playerRayTraceHit.GetActor() )
	{
		playerRayTraceHit = FHitResult();
	}
}


void UGrabber::GrabItem()
{
	// Initialize grabber line trace.
	SetGrabberLineTraceEnd();

	/* After setting grabber line trace end, we need to see if we can hit
	 * a PhysicsBody object within our line trace.
	 * Rather than create 2 FHitResult vars, just create the one we care about
	 * and pass it into GetFirstPhysicsBodyInReach.
	 */
	FHitResult playerHit;
	GetFirstPhysicsBodyInReach(OUT playerHit);

	/* If we have a component on playerHit, then we can safely attach our PhysicsHandle,
	 * which will allow us to translate the Actor the component was found on.
	 */
	if ( playerHit.GetComponent() )
	{
		UPrimitiveComponent* componentToGrab = playerHit.GetComponent();
		
		// Attach Physics Handle to component at GrabberLineTraceEnd
		PhysicsHandle->GrabComponentAtLocation(
			componentToGrab,
			NAME_None,
			GrabberLineTraceEnd
		);
	}
}


void UGrabber::ReleaseItem()
{
	UE_LOG(LogTemp, Warning, TEXT("ReleaseItem() Called!"));

	// Check if the physics handle is applied to an actor currently
	if ( PhysicsHandle->GetGrabbedComponent() )
	{
		PhysicsHandle->ReleaseComponent();
	}
}


// Called every frame
void UGrabber::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	
	// If A Physics Handle is attached
	if ( PhysicsHandle->GrabbedComponent )
	{
		// Move the object we have "grabbed"
		SetGrabberLineTraceEnd();
		PhysicsHandle->SetTargetLocation(GrabberLineTraceEnd * .95f);
	}
}