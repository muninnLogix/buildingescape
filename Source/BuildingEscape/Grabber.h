// Copyright Blasphemer Brainchild Studios, 2022

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "PhysicsEngine/PhysicsHandleComponent.h"
#include "Grabber.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class BUILDINGESCAPE_API UGrabber : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UGrabber();

	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

private:
	// Player Reach Distance
	UPROPERTY(EditAnywhere, Category="Player Reach")
	float PlayerReach = 100.f;

	// Formats UE_LOG entry if a null component pointer is found
	bool IsNullComponent(const void* component, const FString& testingComponent, const FString& ownerName) const;

/* Grabber Component Implementation.
 * Allows a player character to pick up any "PhysicsBody" Collision channel object.
 *
 * Utilizes Ray Tracing and PhysicsHandle Component in order to move objects around.
 */

	// Initialize a Null Physics Handle to avoid Runtime errors.
	UPROPERTY()
	UPhysicsHandleComponent* PhysicsHandle = nullptr;
	// Initialize a Null PlayerInput to avoid Runtime errors.
	UPROPERTY()
	UInputComponent* PlayerInput = nullptr;
	// Player Viewpoint rotation for computing Reach Vector
	FRotator PlayerViewpointRot = FRotator();
	// Viewpoint Location for computing Reach Vector
	FVector PlayerViewpointLoc = FVector();
	// "Endpoint" of the grabber system. Effectively is viewpoint + ( rotation * player reach )
	FVector GrabberLineTraceEnd = FVector();
	/* Apply a physics handle to a valid PhysicsBody object
	 * The presence of a physics handle on an object is checked each tick
	 * and if found will set component actor's translation to the vector of
	 * GrabberLineTraceEnd in order to move the actor.
	 */
	void GrabItem();
	//release the held item
	void ReleaseItem();
	// Calculates the GrabberLineTraceEnd Vector when called based on the Player's current viewpoint, location, and reach.
	void SetGrabberLineTraceEnd();
	/* Intake an FHitResult from GrabItem and set that var to a ray trace hit if one is found.
	 * This returns an empty FHitResult in the event that a ray trace hit DOESN'T intersect a collision object of "PhysicsBody"
	 */
	void GetFirstPhysicsBodyInReach(FHitResult& playerRayTraceHit) const;

/* Establish GrabItem and ReleaseItem functions to fire when "Grab" input is pressed
 * This is a good example of how to map component class functions to fire upon
 * player input
 */
	void ConfigurePlayerInputBindings(UInputComponent* PlayerInputController);
};
