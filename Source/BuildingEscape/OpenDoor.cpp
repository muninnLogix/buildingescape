// Copyright Blasphemer Brainchild Studios, 2022


#include "OpenDoor.h"
#include "Components/AudioComponent.h"
#include "Engine/World.h"
#include "GameFramework/Actor.h"
#include "GameFramework/PlayerController.h"
#include "Math/UnrealMathUtility.h"

#define OUT

// Sets default values for this component's properties
UOpenDoor::UOpenDoor()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
}


void UOpenDoor::BeginPlay()
{
	Super::BeginPlay();
	InitialYaw = GetOwner()->GetActorRotation().Yaw;
	ProtectPressurePlatePtr();
	ConfigureOpenAudio();
}

void UOpenDoor::ProtectPressurePlatePtr() const
{
	if (!PressurePlate)
	{
		UE_LOG(LogTemp, Error, TEXT("Intercepted Null Pointer for required object: PressurePlate"))
		UE_LOG(LogTemp, Error, TEXT("Owner Name: %s"), *GetOwner()->GetName())
		UE_LOG(LogTemp, Error, TEXT("Component: %s"), *GetName())
	}
}

// Called every frame
void UOpenDoor::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if ( PressurePlate && TotalMassOfActors(*PressurePlate) >= OpenDoorMassThreshold )
	{
		OpenDoor(DeltaTime);
		return;
	}


	// Protect from Null Pressure plate
	if ( PressurePlate )
	{
		if ( GetWorld()->GetTimeSeconds() - DoorOpenedTime >= DoorCloseDelay )
		{
			CloseDoor(DeltaTime);
		}
	}

}

void UOpenDoor::OpenDoor(float& DeltaTime)
{
	// Get current Yaw of object
	const float currentYaw = GetOwner()->GetActorRotation().Yaw;
	const float maxYaw = InitialYaw + DoorOpenAngle;

	// Entering function should count as "Door last opened"
	DoorOpenedTime = GetWorld()->GetTimeSeconds();

	// If our door is not "Opened" then we can play our sound
	// Play the sound and flip door open to true
	if ( !bDoorOpen )
	{
		PlayDoorSound();
		bDoorOpen = true;
		return;
	}
	
	if ( currentYaw >= maxYaw * YawOpenCutoff)
	{
		return;
	}

	// lerp from current yaw to max yaw, multiply deltatime
	const FRotator modYaw = {
		0.0f,
		FMath::Lerp(currentYaw, maxYaw, DeltaTime * 2.0f),
		0.0f
	};
	
	GetOwner()->SetActorRotation(modYaw);
}

void UOpenDoor::CloseDoor(float& DeltaTime)
{
	// Get current Yaw of object
	const float currentYaw = GetOwner()->GetActorRotation().Yaw;
	
	// If our door is "Opened" then we can play our sound
	// Play the sound and flip "Opened" to false
	if ( bDoorOpen )
	{
		PlayDoorSound();
		bDoorOpen = false;
		return;
	}
	
	if ( currentYaw <= DoorOpenAngle * ( 1 - YawCloseCutoff))
	{
		return;
	}

	// lerp from current yaw to max yaw, multiply deltatime
	const FRotator modYaw = {
		0.0f,
		FMath::Lerp(currentYaw, InitialYaw, DeltaTime * 4.0f),
		0.0f
	};
	
	GetOwner()->SetActorRotation(modYaw);
}

float UOpenDoor::TotalMassOfActors(const ATriggerVolume& massTriggerVolume) const
{
	float totalMass = 0.f;
	
	// Find All overlapping actors
	TArray<AActor*> actorsOnTrigger;
	massTriggerVolume.GetOverlappingActors(OUT actorsOnTrigger);
	
	// Protect from Null
	if ( actorsOnTrigger.Num() > 0 )
	{
		// Find those actor masses
		for ( AActor* actor: actorsOnTrigger )
		{
			 UPrimitiveComponent* actorPrimitive = actor->FindComponentByClass<UPrimitiveComponent>();
			 totalMass = totalMass + actorPrimitive->GetMass();
			 UE_LOG(LogTemp, Warning, TEXT("Actor: %s, weight: %f"), *actor->GetName(), actorPrimitive->GetMass());
			 UE_LOG(LogTemp, Warning, TEXT("New Total Mass is: %f"), totalMass);
		}
	}
	
	return totalMass;
}

void UOpenDoor::ConfigureOpenAudio() 
{
	DoorOpenAudioComponent = GetOwner()->FindComponentByClass<UAudioComponent>();
	if ( !DoorOpenAudioComponent )
	{
		UE_LOG(LogTemp, Error, TEXT("Component %s not found on Owner: %s"),
			*FString("Door Audio Component"),
			*GetOwner()->GetName())
		
		return;
	}

	UE_LOG(LogTemp, Warning, TEXT("Initialized Door Audio Component"));
}

void UOpenDoor::PlayDoorSound() const
{
	if ( !DoorOpenAudioComponent ) {return;}
	DoorOpenAudioComponent->Play();
}
