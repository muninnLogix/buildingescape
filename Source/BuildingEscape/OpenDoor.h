// Copyright Blasphemer Brainchild Studios, 2022

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Engine/StaticMeshActor.h"
#include "Engine/TriggerVolume.h"
#include "OpenDoor.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class BUILDINGESCAPE_API UOpenDoor : public UActorComponent
{
	GENERATED_BODY()


public:	
	// Sets default values for this component's properties
	UOpenDoor();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	// Opens Door with appropriate lerp
	void OpenDoor(float& DeltaTime);

	// Closes Door with appropriate lerp
	void CloseDoor(float& DeltaTime);

	// Tallies up the mass of all actors on the associated trigger volume
	float TotalMassOfActors(const ATriggerVolume& massTriggerVolume) const;

private:
	// Allow Editing of Max Door Yaw
	UPROPERTY(EditAnywhere, Category="Door")
	float DoorOpenAngle = 90.f;
	
	// Delay in Seconds to keep the door opened before closing
	UPROPERTY(EditAnywhere, Category="Door")
	float DoorCloseDelay = 0.f;

	// What percantage of "Open" to stop moving the door at.
	UPROPERTY(EditAnywhere, Category="Door")
	float YawOpenCutoff = .95f;

	// What percantage of "Closed" to stop moving the door at.
	UPROPERTY(EditAnywhere, Category="Door")
	float YawCloseCutoff = .95f;

	// Links a Pressure Plate volume up with opening the Door
	UPROPERTY(EditAnywhere, Category="Door")
	ATriggerVolume* PressurePlate = nullptr;

	// Set Static Mesh that can open door
	UPROPERTY(EditAnywhere, Category="Door")
	AStaticMeshActor* StaticActorThatOpens = nullptr;

	// Set Player Pawn that triggers open
	UPROPERTY(EditAnywhere, Category="Door")
	float OpenDoorMassThreshold = 100.f;

	void ConfigureOpenAudio();
	void PlayDoorSound() const;
	void ProtectPressurePlatePtr() const;
	
	/** Non-UPROPERTY items
	 ********************************************************************************/

	// Initial setting of the Door Yaw for relative open/close
	float InitialYaw;

	// When the door was opened to allow for delay
	float DoorOpenedTime = 0.f;

	UPROPERTY()
	UAudioComponent* DoorOpenAudioComponent = nullptr;

	bool bDoorOpen = false;
};
